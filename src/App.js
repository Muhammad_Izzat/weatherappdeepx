import React from 'react'
import logo from './logo.svg';
import './App.css';
import CityForm from './components/input-form.js'
import DisplayWeather from './components/display-weather.js'
import Rain from './media/weather-icon/rain.png'
import Clouds from './media/weather-icon/clouds.png'
import BrokenClouds from './media/weather-icon/broken_clouds.png'
import ClearSky from './media/weather-icon/clear_sky.png'
import FewClouds from './media/weather-icon/few_clouds.png'
import ScatteredClouds from './media/weather-icon/scattered_clouds.png'
import ShowerRain from './media/weather-icon/shower_rain.png'
import Thunderstorm from './media/weather-icon/thunderstorm.png'

const API_KEY = '0b86c9c5df7e67b31bdea7637e28ed25';

const weather_box_style = {
    margin: "auto",
    width: "30%",
}
class App extends React.Component {
    state = {
        temperature : undefined,
        city: undefined,
        description: undefined,
        error: undefined
    }

    getWeather = async (event) => {
        event.preventDefault();

        const city = event.target.elements.city.value;
        // const country = event.target.elements.country.value;
        const api_call = await fetch (`https://api.openweathermap.org/data/2.5/weather?q=${city},my&appid=${API_KEY}&units=metric`);
        
        const data = await api_call.json();
        if(data.cod !== '404'){
            if(data.weather[0].main == 'Rain'){
                this.setState({
                    temperature: data.main.temp + ' °C',
                    city: data.name,
                    description: data.weather[0].main,
                    weather_image: Rain,
                    error: ""
                });
            }
            else if(data.weather[0].main == 'Clouds'){
                this.setState({
                    temperature: data.main.temp + ' °C',
                    city: data.name,
                    description: data.weather[0].main,
                    weather_image: Clouds,
                    error: ""
                });
            }
            else if(data.weather[0].main == 'Broken Clouds'){
                this.setState({
                    temperature: data.main.temp + ' °C',
                    city: data.name,
                    description: data.weather[0].main,
                    weather_image: BrokenClouds,
                    error: ""
                });
            }
            else if(data.weather[0].main == 'Clear Sky'){
                this.setState({
                    temperature: data.main.temp + ' °C',
                    city: data.name,
                    description: data.weather[0].main,
                    weather_image: ClearSky,
                    error: ""
                });
            }
            else if(data.weather[0].main == 'Few Clouds'){
                this.setState({
                    temperature: data.main.temp + ' °C',
                    city: data.name,
                    description: data.weather[0].main,
                    weather_image: FewClouds,
                    error: ""
                });
            }
            else if(data.weather[0].main == 'Scattered Clouds'){
                this.setState({
                    temperature: data.main.temp + ' °C',
                    city: data.name,
                    description: data.weather[0].main,
                    weather_image: ScatteredClouds,
                    error: ""
                });
            }
            else if(data.weather[0].main == 'Shower Rain'){
                this.setState({
                    temperature: data.main.temp + ' °C',
                    city: data.name,
                    description: data.weather[0].main,
                    weather_image: ShowerRain,
                    error: ""
                });
            }
            else if(data.weather[0].main == 'Thunderstorm'){
                this.setState({
                    temperature: data.main.temp + ' °C',
                    city: data.name,
                    description: data.weather[0].main,
                    weather_image: Thunderstorm,
                    error: ""
                });
            }
        } else{
            this.setState({
                temperature: undefined,
                city: undefined,
                description: undefined,
                error: "Please enter a proper city name in Malaysia"
            });
        }
    }
    
    render(){
        return(
            <div className="App" >
                <div className="row" style={weather_box_style}>
                    <DisplayWeather cityName={this.state.city} weatherImg={this.state.weather_image} weatherTemp={this.state.temperature} weatherCond={this.state.description} error={this.state.error}></DisplayWeather>
                </div>
                <br></br>
                <div className="row">
                    <CityForm getWeather={this.getWeather}></CityForm>
                </div>
            </div>
        )
    }
}

export default App;
