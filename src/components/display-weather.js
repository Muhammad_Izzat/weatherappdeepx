import React from 'react'

// class DisplayWeather extends React.Component{
//     render(){
//         return()
//     }
// }

const DisplayWeather = props => {
    return (<div>
                {props.cityName && 
                    <div style={{display:'block'}}>
                        <div>
                            <h1>{props.cityName}</h1>
                            <img src={props.weatherImg} style={{height:'75px'}}></img>
                        </div>
                        <div>
                            <ul style={{paddingLeft: '0px'}}>
                            <li style={{display:'inline-block', width:'auto', float:'left'}}>{props.weatherTemp}</li>
                            <li style={{display:'inline-block', width:'auto', float:'right'}}>{props.weatherCond}</li>
                            </ul> 
                        </div>
                    </div>
                }
                {props.error &&
                    <div style={{display:'block'}}>
                        <p>{props.error}</p>
                    </div>
                } 
            </div>
    )
}

export default DisplayWeather