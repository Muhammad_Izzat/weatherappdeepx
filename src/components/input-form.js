import React from 'react'

const CityForm = props =>{
   return(<div className="row">
                <form onSubmit={props.getWeather}>
                    <input type="text" name="city" placeholder="Insert a City"></input>
                    {/* <input type="text" name="country" placeholder="Insert a Country"></input> */}
                    <button>View weather</button>
                </form>
            </div>
   )
}

export default CityForm